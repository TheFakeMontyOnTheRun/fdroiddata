Categories:Games
License:GPLv3+
Web Site:https://baiatbun.github.io/React/
Source Code:https://github.com/baiatBun/React
Issue Tracker:https://github.com/baiatBun/React/issues
Changelog:https://github.com/baiatBun/React/releases

Auto Name:React
Summary:The application tests your reaction time
Description:
A simple application that tests your reaction time.
.

Repo Type:git
Repo:https://github.com/baiatBun/React.git

Build:1.2.1,1
    commit=v1.2.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2.1
Current Version Code:1
