Categories:Multimedia,System
License:GPLv3
Web Site:https://www.secuso.informatik.tu-darmstadt.de/en/secuso-home/
Source Code:https://github.com/SecUSo/privacy-friendly-qr-scanner
Issue Tracker:https://github.com/SecUSo/privacy-friendly-qr-scanner/issues

Auto Name:QR Scanner
Summary:Scan QR codes
Description:
Scanner that supports its users in detecting malicious links: QR codes provide
new possibilities for an attacker, as they can contain malicious links, i.e.
links to phishing webpages or webpages from which malware would automatically be
downloaded. Therefore it is important to carefully check the link before
accessing the corresponding webpage. It belongs to the group of Privacy Friendly
Apps from the research group SecUSo (Security, Usability and Society) by the
Technische Universität Darmstadt, Germany.
.

Repo Type:git
Repo:https://github.com/SecUSo/privacy-friendly-qr-scanner

Build:1.5,6
    commit=b918951037727e542355844a51e96cbae3913b45
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/maven {/,+2d' ../build.gradle

Maintainer Notes:
Tags are not up-to-date, package id might change like it did with other SECUSO apps.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4
Current Version Code:5
